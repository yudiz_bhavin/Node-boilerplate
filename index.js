/**
 * node modules import.
 */
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const express = require('express');
const expressValidator = require('express-validator');
const mongoose = require('mongoose');
const path = require('path');
const helmet = require('helmet');

/**
 * internal modules/files import
 */
const config = require('./config/default');

const app = express();
const port = config.port;

/**
 * middlewares
 */
app.use(cors());
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(expressValidator());

/**
 * mongoose connection (url from config/env)
 */
mongoose.connect(config.MONGODB_URL, { promiseLibrary: global.Promise }, function (error) {
  if (error) {
    console.log('database not connected', error);
  } else {
    console.log('database connected');
  }
});
mongoose.set('debug', true);
/**
 * routes
 */
const userRoutes = require('./routes/v1/user.routes');
app.use('/api/v1/user', userRoutes);

const authRoutes = require('./routes/v1/auth.routes');
app.use('/api/v1/auth', authRoutes);

app.listen(port, () => {
  console.log('Magic happens on port :' + port)
});