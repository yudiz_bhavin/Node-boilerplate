const userModel = require('./../models/user.model');
const status = require('./../messages/status.json');
const messages = require('./../messages/user.response.json');

const isAuthenticated = (req, res, next) => {
  var token = req.header('Authorization');
  userModel.findByToken(token).then((user) => {
    if (!user) return res.status(status.Unauthorized).jsonp({ "error": messages.err_unauthorized });

    req._id = user._id;
    return next(null, null);
  }).catch((error) => { 
    return res.status(401).jsonp({ "error": messages.err_unauthorized });
  });
}

module.exports = isAuthenticated;