const _ = require('lodash');
const bcrypt = require('bcrypt-nodejs');
const fs = require('fs');
const jwt = require("jsonwebtoken");

const userModel = require('./../../models/user.model');
const status = require('./../../messages/status.json');
const messages = require('./../../messages/user.response.json');
const config = require('./../../config/default');

class Auth {

  async login(req, res) {
    try {
      req.checkBody("sEmail", messages.req_email).notEmpty();
      req.checkBody("sPassword", messages.req_password).notEmpty();

      const result = await req.getValidationResult();
      if (!result.isEmpty()) return res.status(status.BadRequest).json(result.array());

      const body = _.pick(req.body, ['sEmail', 'sPassword']);

      userModel.findOne({ sEmail: body.sEmail }, (error, user) => {
        if (error) return res.status(status.InternalServerError).jsonp({ "message": error });

        if (!user) return res.status(status.Locked).jsonp({ "message": messages.user_not_found });


        if (!bcrypt.compareSync(body.sPassword, user.sPassword)) {
          return res.status(status.BadRequest).json({ message: messages.wrong_password });
        }

        const cert = fs.readFileSync(config.base_path + "/storage/" + process.env.PRIVATE_KEY);

        user.sJwtToken = jwt.sign({ _id: (user._id).toHexString() }, cert);

        user.save().then((data) => {
          res.status(status.OK).jsonp({
            message: messages.succ_login,
            Authorization: data.sJwtToken
          });
        });
      })

    } catch (e) {
      console.log(`Error :: ${e}`);
      let err = `Error :: ${e}`;
      return res.status(status.InternalServerError).json({ message: messages.error, error: err });
    }
  }

  async logout(req, res) {
    try {
      userModel.findOne({ _id: req._id }, (error, data) => {
        if (error) return res.status(status.InternalServerError).jsonp({ "message": error });

        if (!user) return res.status(status.Locked).jsonp({ "message": messages.user_not_found });

        user.sJwtToken = ""; 
        user.save().then(() => {
          res.status(status.OK).jsonp({ message: messages.succ_logout });
        })
      });
    } catch (e) {
      console.log(`Error :: ${e}`);
      let err = `Error :: ${e}`;
      return res.status(status.InternalServerError).json({ message: messages.error, error: err });
    }
  }

}

module.exports = new Auth();