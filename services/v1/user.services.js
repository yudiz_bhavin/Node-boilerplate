const _ = require('lodash');
const fs = require("fs");
const jwt = require("jsonwebtoken");
const userModel = require('./../../models/user.model');
const status = require('./../../messages/status.json');
const messages = require('./../../messages/user.response.json');
const config = require('./../../config/default');
const { removenull } = require('./../../utility/removeNull');
const { sendmail } = require('./../../utility/mail');

class User {

  async store(req, res) {
    try {

      req.checkBody("sEmail", messages.req_email).notEmpty();
      req.checkBody("sPassword", messages.req_password).notEmpty();

      let result = await req.getValidationResult();
      if (!result.isEmpty()) return res.status(status.BadRequest).json(result.array());

      let body = _.pick(req.body, ['sEmail', 'sPassword']);

      let params = {
        sEmail: body.sEmail,
        sPassword: body.sPassword,
      }
      removenull(params);
      sendmail({
        from: "bhavin",
        to: "", // list of receivers
        subject: "", // Subject line
        html: "<h1></h1>"
      });

      let newUser = new userModel(params);

      newUser.save().then((data) => {
        return res.status(status.OK).jsonp({ data })
      }).catch((error) => {
        return res.status(status.InternalServerError).json({ error });
      });
    } catch (e) {
      console.log(`Error :: ${e}`);
      let err = `Error :: ${e}`;
      return res.status(status.InternalServerError).json({ message: messages.error, error: err });
    }
  }

  async show(req, res) {
    try {
      if (!req.params.id) {
        return res.status().jsonp({});
      }

      const id = req.params.id;

      userModel.findOne({ _id: id }, (error, data) => {
        if (error) return res.status(status.InternalServerError).jsonp({ "message": error });

        if (!data) return res.status(status.Locked).jsonp({ "message": messages.user_not_found });

        return res.status(status.OK).jsonp({
          "message": messages.succ_user_data,
          data
        });
      });

    } catch (e) {
      console.log(`Error :: ${e}`);
      let err = `Error :: ${e}`;
      return res.status(status.InternalServerError).json({ message: messages.error, error: err });
    }
  }

  async update(req, res) {

  }

  async delete(req, res) {

  }

  async list(req, res) {

  }

}

module.exports = new User();