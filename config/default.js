require("dotenv").config();
const path = require("path");

const APP_NAME = process.env.APP_NAME || 'Node-Boilerplate';
const BASE_PATH = path.join(__dirname + "./../");
const PORT = process.env.PORT || 3000;
const BASE_URL = (process.env.APP_URL || "http://localhost") + ":" + PORT;
const FROM_EMAIL = '"' + APP_NAME + '" <' + process.env.SMTP_FROM || "abc@xyz.com" + '>'
const MONGODB_URL = process.env.MONGO_URL || 'mongodb://127.0.0.1:27017/';

module.exports = {
  port: PORT,
  base_path: BASE_PATH,
  base_url: BASE_URL,
  app_name: APP_NAME,
  FROM_EMAIL: FROM_EMAIL,
  MONGODB_URL: MONGODB_URL
}