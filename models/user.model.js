const fs = require("fs");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt-nodejs");
const jwt = require("jsonwebtoken");
const saltRounds = 10;
const salt = bcrypt.genSaltSync(saltRounds);

const config = require("./../config/default");

const cert_public = fs.readFileSync(config.base_path + "/storage/" + process.env.PRIVATE_KEY);

const Schema = mongoose.Schema;

const Users = new Schema({
  sEmail: { type: String },
  sPassword: { type: String },
  Isactive: { type: Boolean, enum: [true, false] },
  sJwtToken: { type: String },
  dCreatedDate: { type: Date, default: Date.now }
});

Users.pre('save', function (next) {
  var user = this;
  if (user.isModified('sPassword')) {
    user.sPassword = bcrypt.hashSync(user.sPassword, salt);
  }
  next();
});
 
Users.statics.findByToken = function (token) {
  var User = this;
  var decoded;
  try { 
    decoded = jwt.verify(token, cert_public); 
  } catch (e) {
    return new Promise((resolve, reject) => {
      reject();
    });
  }
  var query = {
    '_id': decoded._id,
    'sJwtToken': token
  };
  return User.findOne(query);
};

module.exports = mongoose.model('users', Users);