const router = require("express").Router();
const service = require('./../../services/v1/auth.services');
const authMiddleware = require('./../../middlewares/auth.middleware');


router.post('/login',service.login);
router.use(authMiddleware);
router.get('/logout',service.logout);

module.exports = router;