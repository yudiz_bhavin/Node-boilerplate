const router = require("express").Router();
const service = require('./../../services/v1/user.services');
const authMiddleware = require('./../../middlewares/auth.middleware');

/**
 * routes without authentication
 */
router.post('/', service.store);

/**
 * authentication middleware
 */
router.use(authMiddleware);

/**
 * routes with authentication
 */
router.get('/:id', service.show);
router.put('/:id', service.update);
router.delete('/:id', service.delete);
router.post('/list', service.list);

module.exports = router;