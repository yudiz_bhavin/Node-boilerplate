const nodemailer = require('nodemailer');
let transporter = nodemailer.createTransport({
  host: process.env.SMTP_HOST || 'gator3109.hostgator.com',
  port: process.env.SMTP_PORT || 465,
  auth: {
    user: process.env.SMTP_USERNAME || 'xyz@xyz.xyz', // SMTP email
    pass: process.env.SMTP_PASSWORD || 'xyz' // Your password
  },
  secure: true
});

const sendmail = (mailOption) => { 
  return transporter.sendMail(mailOption).then(function (success) {
    // return success.messageId;
    transporter.close();
  }).catch(function (err) {
    //return err;
    console.log(err);
  });
};

module.exports = {
  sendmail
}



// sendmail({
//   from: "",
//   to: "", // list of receivers
//   subject: "", // Subject line
//   html: "<h1></h1>"
// });